#Compile Sparnas_decoder
FROM alpine as SPARSNAS_BUILD_ENV

RUN apk add --update --no-cache \
    g++ \
    make \ 
    mosquitto-dev 

COPY Makefile /build/
COPY sparsnas_decode.cpp /build/

WORKDIR /build
RUN make


#Assemble the final image
FROM alpine

RUN apk add --update --no-cache \
    zsh \ 
    rtl-sdr \
    mosquitto-libs++

COPY --from=SPARSNAS_BUILD_ENV /build/sparsnas_decode /usr/bin/
COPY run_sparsnas.sh /usr/bin

RUN chmod 755 /usr/bin/sparsnas_decode
RUN mkdir /config

ENTRYPOINT ["/usr/bin/run_sparsnas.sh"]
